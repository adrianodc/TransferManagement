package com.uncaffeperdue.transfermanagement.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class Transference {
	private Account receiver;
	private Account sender;
	private BigDecimal value;
	private OffsetDateTime date;
	public Account getReceiver() {
		return receiver;
	}
	public void setReceiver(Account receiver) {
		this.receiver = receiver;
	}
	public Account getSender() {
		return sender;
	}
	public void setSender(Account sender) {
		this.sender = sender;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public OffsetDateTime getDate() {
		return date;
	}
	public void setDate(OffsetDateTime date) {
		this.date = date;
	}
}
