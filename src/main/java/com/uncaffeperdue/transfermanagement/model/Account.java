package com.uncaffeperdue.transfermanagement.model;

import java.math.BigDecimal;

public class Account {
	private String accountNumber;

	private BigDecimal currentValue;

	private String accountOwner;

	private String accountOwnerId;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(BigDecimal currentValue) {
		this.currentValue = currentValue;
	}

	public String getAccountOwner() {
		return accountOwner;
	}

	public void setAccountOwner(String accountOwner) {
		this.accountOwner = accountOwner;
	}

	public String getAccountOwnerId() {
		return accountOwnerId;
	}

	public void setAccountOwnerId(String accountOwnerId) {
		this.accountOwnerId = accountOwnerId;
	}
	
	public BigDecimal updateValue(BigDecimal value) {
		if(value != null)
			this.currentValue = currentValue.add(value);
		return this.currentValue;
	}
}
