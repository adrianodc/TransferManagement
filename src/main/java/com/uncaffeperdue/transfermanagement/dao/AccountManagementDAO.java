package com.uncaffeperdue.transfermanagement.dao;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.uncaffeperdue.transfermanagement.SystemUtils;
import com.uncaffeperdue.transfermanagement.dto.AccountDTO;
import com.uncaffeperdue.transfermanagement.dto.TransferenceDTO;
import com.uncaffeperdue.transfermanagement.model.Account;
import com.uncaffeperdue.transfermanagement.model.Transference;

public class AccountManagementDAO {
	private static AccountManagementDAO instance = null;
	private Map<String, Account> accounts;
	private Map<String, String> accountsByOwnerID;
	private Map<String, Transference> transferences;
    private AccountManagementDAO() {
    	accounts = new HashMap<>();
    	accountsByOwnerID = new HashMap<>();
    	this.transferences = new HashMap<>();
    }

    public static synchronized AccountManagementDAO getInstance() {
        if (instance == null) {
            instance = new AccountManagementDAO();
        }
        return instance;
    }

	public AccountDTO createAccount(AccountDTO account) {
		validateAccount(account);
		if(instance.accountsByOwnerID.containsKey(account.getAccountOwnerId()))
			return null;
		Account newAccount = SystemUtils.fromDTO(account);
		newAccount.setAccountNumber(SystemUtils.generateUUID());
		instance.accountsByOwnerID.put(account.getAccountOwnerId(), newAccount.getAccountNumber());
		instance.accounts.put(newAccount.getAccountNumber(), newAccount);
		return SystemUtils.toDTO(newAccount);
	}
	
	public Account findAccount(String accountNumber) {
		return instance.accounts.get(accountNumber);
	}
	
	public Collection<Account> getAllAccounts(){
		return instance.accounts.values();
	}
	
	public TransferenceDTO transferMoney(TransferenceDTO dto) {
		validateTransference(dto);
		Account receiver = findAccount(dto.getReceiverAccount());
		Account sender = findAccount(dto.getSenderAccount());
		if(receiver != null && sender != null) {
			BigDecimal value = new BigDecimal(dto.getValue());
			if(sender.getCurrentValue().compareTo(value) >= 0) {
				Transference transference = SystemUtils.fromDTO(dto, this);
				instance.transferences.put(SystemUtils.generateUUID(), transference);
				receiver.updateValue(transference.getValue());
				sender.updateValue(value.negate());
				transference.setDate(OffsetDateTime.now());
				return SystemUtils.toDTO(transference);
			} else {
				throw new IllegalArgumentException("You have not enough money do proceed with this transference.");
			}
		}
		throw new IllegalArgumentException("Invalid data.");
	}
	
	public List<Transference> getMovements(String accountNumber) {
		return instance.transferences.values().stream().filter(t -> (t.getReceiver().getAccountNumber().equals(accountNumber)
				|| t.getSender().getAccountNumber().equals(accountNumber)))
			.sorted((a,b) -> a.getDate().compareTo(b.getDate()))
			.collect(Collectors.toList());
	}
	
	private void validateAccount(AccountDTO dto) {
		if(dto.getAccountOwnerId() == null) {
			throw new IllegalArgumentException("It is not possible to create an account without the ID of the owner.");
		}
		if(dto.getAccountOwner() == null){
			throw new IllegalArgumentException("It is not possible to create an account without the name of the owner.");
		}
		if(dto.getCurrentValue() <= 0.0d){
			throw new IllegalArgumentException("It is not possible to create an account with negative initial value.");
		}
	}
	
	private void validateTransference(TransferenceDTO dto) {
		if(dto.getSenderAccount() == null) {
			throw new IllegalStateException("The account number of the origin of the transference must be a valid value.");
		}
		if(dto.getReceiverAccount() == null) {
			throw new IllegalStateException("The account number of the destination of the transference must be a valid value.");
		}
		if(dto.getValue() <= 0.0d){
			throw new IllegalArgumentException("The value must be greater than zero.");
		}
	}
}
