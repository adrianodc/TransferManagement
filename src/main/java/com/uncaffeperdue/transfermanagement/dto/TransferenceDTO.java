package com.uncaffeperdue.transfermanagement.dto;

import java.time.OffsetDateTime;

public class TransferenceDTO {
	private String receiverAccount;
	private String receiverName;
	private String senderAccount;
	private String senderName;
	private double accountBalance;
	private double value;
	private OffsetDateTime date;
//	public AccountDTO getReceiver() {
//		return receiver;
//	}
//	public void setReceiver(AccountDTO receiver) {
//		this.receiver = receiver;
//	}
//	public AccountDTO getSender() {
//		return sender;
//	}
//	public void setSender(AccountDTO sender) {
//		this.sender = sender;
//	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public OffsetDateTime getDate() {
		return date;
	}
	public void setDate(OffsetDateTime date) {
		this.date = date;
	}
	public double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	public String getReceiverAccount() {
		return receiverAccount;
	}
	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}
	public String getSenderAccount() {
		return senderAccount;
	}
	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}
	public String getReceiverName() {
		return receiverName;
	}
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
}
