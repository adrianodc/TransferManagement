package com.uncaffeperdue.transfermanagement;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.uncaffeperdue.transfermanagement.controller.AccountController;
import com.uncaffeperdue.transfermanagement.controller.TransferenceController;

public class TransferServer {

	public static void main(String[] args) throws IOException {
		ResourceConfig config = new ResourceConfig().addClasses(AccountController.class, TransferenceController.class);
		URI uri = URI.create("http://localhost:8080/");
		HttpServer server = GrizzlyHttpServerFactory.createHttpServer(uri, config);
		System.in.read();
		server.stop();
	}

}
