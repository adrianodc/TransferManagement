package com.uncaffeperdue.transfermanagement.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.uncaffeperdue.transfermanagement.SystemUtils;
import com.uncaffeperdue.transfermanagement.dao.AccountManagementDAO;
import com.uncaffeperdue.transfermanagement.dto.TransferenceDTO;

@Path(value="transferences")
public class TransferenceController {
	@POST
	@Path("/transfer")
	@Produces(MediaType.APPLICATION_JSON)
	public Response transferMoney(TransferenceDTO transference) {
		AccountManagementDAO dao = AccountManagementDAO.getInstance();
		TransferenceDTO dto = dao.transferMoney(transference);
		if(dto == null) {
	        return Response.status(Response.Status.BAD_REQUEST).entity("It is not possible to transfer this money. Check your balance and the account number of the receiver.").build();
	    }
	    return Response.ok(SystemUtils.getJSONBuilder().toJson(dto), MediaType.APPLICATION_JSON).build();
	}
	
	@GET
	@Path("/getbalance/{accountNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBalance(@PathParam("accountNumber") String accountNumber) {
		AccountManagementDAO dao = AccountManagementDAO.getInstance();
		List<TransferenceDTO> transferences = dao.getMovements(accountNumber).stream().map(a -> SystemUtils.toDTO(a)).collect(Collectors.toList());
		return SystemUtils.getJSONBuilder().toJson(transferences);
	}
}
