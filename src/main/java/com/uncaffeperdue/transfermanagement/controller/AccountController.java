package com.uncaffeperdue.transfermanagement.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.uncaffeperdue.transfermanagement.SystemUtils;
import com.uncaffeperdue.transfermanagement.dao.AccountManagementDAO;
import com.uncaffeperdue.transfermanagement.dto.AccountDTO;
import com.uncaffeperdue.transfermanagement.model.Account;

@Path(value="accounts")
public class AccountController {
	

	@POST
	@Path("/createAccount")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createAccount(AccountDTO account) {
		AccountManagementDAO dao = AccountManagementDAO.getInstance();
		AccountDTO dto = dao.createAccount(account);
		if(dto == null) {
	        return Response.status(Response.Status.BAD_REQUEST).entity(String.format("It is not possible to create an account for the owner with ID %s", account.getAccountOwnerId())).build();
	    }
	    return Response.ok(SystemUtils.getJSONBuilder().toJson(dto), MediaType.APPLICATION_JSON).build();
	}
	
	@POST
	@Path("/createAccount/{accountNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAccount(@PathParam("accountNumber") String accountNumber) {
		AccountManagementDAO dao = AccountManagementDAO.getInstance();
		Account account = dao.findAccount(accountNumber);
		if(account == null) {
	        return Response.status(Response.Status.BAD_REQUEST).entity(String.format("It was not possible to find account with number %s", accountNumber)).build();
	    }
	    return Response.ok(SystemUtils.getJSONBuilder().toJson(SystemUtils.toDTO(account)), MediaType.APPLICATION_JSON).build();
	}
	
	@GET
	@Path("/allaccounts")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAccounts() {
		AccountManagementDAO dao = AccountManagementDAO.getInstance();
		List<AccountDTO> allAccounts = dao.getAllAccounts().stream().map(a -> SystemUtils.toDTO(a)).collect(Collectors.toList());
		return SystemUtils.getJSONBuilder().toJson(allAccounts);
	}
}
