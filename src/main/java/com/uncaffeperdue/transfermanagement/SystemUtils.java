package com.uncaffeperdue.transfermanagement;

import java.math.BigDecimal;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.uncaffeperdue.transfermanagement.dao.AccountManagementDAO;
import com.uncaffeperdue.transfermanagement.dto.AccountDTO;
import com.uncaffeperdue.transfermanagement.dto.TransferenceDTO;
import com.uncaffeperdue.transfermanagement.model.Account;
import com.uncaffeperdue.transfermanagement.model.Transference;

public class SystemUtils {
	public static Gson getJSONBuilder() {
		return new GsonBuilder().setPrettyPrinting().create();
	}
	
	public static String generateUUID() {
		return UUID.randomUUID().toString();
	}
	
	public static AccountDTO toDTO(Account from) {
		AccountDTO to = new AccountDTO();
		to.setAccountNumber(from.getAccountNumber());
		to.setAccountOwner(from.getAccountOwner());
		to.setAccountOwnerId(from.getAccountOwnerId());
		to.setCurrentValue(from.getCurrentValue().doubleValue());
		
		return to;
	}
	
	public static Account fromDTO(AccountDTO from) {
		Account to = new Account();
		to.setAccountNumber(from.getAccountNumber());
		to.setAccountOwner(from.getAccountOwner());
		to.setAccountOwnerId(from.getAccountOwnerId());
		to.setCurrentValue(new BigDecimal(from.getCurrentValue()));
		
		return to;
	}
	
	public static Transference fromDTO(TransferenceDTO from, AccountManagementDAO dao) {
		Transference to = new Transference();
		to.setDate(from.getDate());
		to.setReceiver(dao.findAccount(from.getReceiverAccount()));
		to.setSender(dao.findAccount(from.getSenderAccount()));
		to.setValue(new BigDecimal(from.getValue()));
		return to;
	}
	
	public static TransferenceDTO toDTO(Transference from) {
		TransferenceDTO to = new TransferenceDTO();
		to.setDate(from.getDate());
		to.setReceiverAccount(from.getReceiver().getAccountNumber());
		to.setReceiverName(from.getReceiver().getAccountOwner());
		to.setSenderAccount(from.getSender().getAccountNumber());
		to.setSenderName(from.getSender().getAccountOwner());
		to.setValue(from.getValue().doubleValue());
		to.setAccountBalance(from.getSender().getCurrentValue().doubleValue());
		return to;
	}
}
