package com.uncaffeperdue.transfermanagement;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.uncaffeperdue.transfermanagement.dao.AccountManagementDAO;
import com.uncaffeperdue.transfermanagement.dto.AccountDTO;
import com.uncaffeperdue.transfermanagement.dto.TransferenceDTO;
import com.uncaffeperdue.transfermanagement.model.Account;
import com.uncaffeperdue.transfermanagement.model.Transference;


public class TransferServerUnitTest
{
	@Test
    public void testCreationOfAccount()
    {
		AccountManagementDAO accountDAO = AccountManagementDAO.getInstance();
		String ACCOUNT_OWNER = "Jay Surname";
		AccountDTO account = createAccountDTO(ACCOUNT_OWNER, "Doc Jay", 15.0d);
    	AccountDTO accountDTO = accountDAO.createAccount(account);
    	assertTrue(accountDTO.getAccountOwner().equals(ACCOUNT_OWNER));
    }
	
	@Test(expected=IllegalArgumentException.class)
    public void testCreationOfAccountWithInvalidArgument()
    {
		AccountManagementDAO accountDAO = AccountManagementDAO.getInstance();
		String ACCOUNT_OWNER = "John Surname";
		AccountDTO account = createAccountDTO(ACCOUNT_OWNER, "Doc John", -1.0d);
    	AccountDTO accountDTO = accountDAO.createAccount(account);
    	assertTrue(accountDTO.getAccountOwner().equals(ACCOUNT_OWNER));
    }
	
	@Test
    public void testCreationOfTwoAccountsAndTransfer()
    {
		AccountManagementDAO accountDAO = AccountManagementDAO.getInstance();
		String ACCOUNT_OWNER1 = "Paul Surname";
		AccountDTO account1 = createAccountDTO(ACCOUNT_OWNER1, "Doc Paul", 15.0d);
		String ACCOUNT_OWNER2 = "Cesar Surname";
		AccountDTO account2 = createAccountDTO(ACCOUNT_OWNER2, "Doc Cesar", 15.0d);
		AccountDTO receiver = accountDAO.createAccount(account1);
		AccountDTO sender = accountDAO.createAccount(account2);
		TransferenceDTO transferenceDTO = createTransferenceDTO(sender, receiver, 5.0d);
		TransferenceDTO newTransference = accountDAO.transferMoney(transferenceDTO);
    	assertTrue(newTransference.getReceiverName().equals(receiver.getAccountOwner())
    			&& newTransference.getSenderName().equals(sender.getAccountOwner())
    			&& newTransference.getValue() == transferenceDTO.getValue());
    }
	
	@Test(expected=IllegalArgumentException.class)
    public void testTransferWithValueGreaterThanBalance()
    {
		AccountManagementDAO accountDAO = AccountManagementDAO.getInstance();
		String ACCOUNT_OWNER1 = "Raphael Surname";
		AccountDTO account1 = createAccountDTO(ACCOUNT_OWNER1, "Doc Raphael", 15.0d);
		String ACCOUNT_OWNER2 = "Eliah Surname";
		AccountDTO account2 = createAccountDTO(ACCOUNT_OWNER2, "Doc Eliah", 15.0d);
		AccountDTO receiver = accountDAO.createAccount(account1);
		AccountDTO sender = accountDAO.createAccount(account2);
		TransferenceDTO transferenceDTO = createTransferenceDTO(sender, receiver, 30.0d);
		TransferenceDTO newTransference = accountDAO.transferMoney(transferenceDTO);
    	assertTrue(newTransference.getReceiverName().equals(receiver.getAccountOwner())
    			&& newTransference.getSenderName().equals(sender.getAccountOwner())
    			&& newTransference.getValue() == transferenceDTO.getValue());
    }
	
	@Test
    public void testCreationOfTwoTransferences()
    {
		AccountManagementDAO accountDAO = AccountManagementDAO.getInstance();
		String ACCOUNT_OWNER1 = "Mary Surname";
		AccountDTO account1 = createAccountDTO(ACCOUNT_OWNER1, "Doc Mary", 15.0d);
		String ACCOUNT_OWNER2 = "Peter Surname";
		AccountDTO account2 = createAccountDTO(ACCOUNT_OWNER2, "Doc Peter", 15.0d);
		AccountDTO mary = accountDAO.createAccount(account1);
		AccountDTO peter = accountDAO.createAccount(account2);
		TransferenceDTO transferenceDTO1 = createTransferenceDTO(mary, peter, 5.0d);
		TransferenceDTO transferenceDTO2 = createTransferenceDTO(peter, mary, 2.0d);
		accountDAO.transferMoney(transferenceDTO1);
		accountDAO.transferMoney(transferenceDTO2);
		List<Transference> movements = accountDAO.getMovements(mary.getAccountNumber());
    	assertTrue(movements.size() == 2);
    }
	
	@Test
    public void testChekBalance()
    {
		AccountManagementDAO accountDAO = AccountManagementDAO.getInstance();
		String ACCOUNT_OWNER1 = "Mark Surname";
		AccountDTO account1 = createAccountDTO(ACCOUNT_OWNER1, "Doc Mark", 15.0d);
		String ACCOUNT_OWNER2 = "Adrian Surname";
		AccountDTO account2 = createAccountDTO(ACCOUNT_OWNER2, "Doc Adrian", 15.0d);
		AccountDTO mark = accountDAO.createAccount(account1);
		AccountDTO adrian = accountDAO.createAccount(account2);
		TransferenceDTO transferenceDTO1 = createTransferenceDTO(mark, adrian, 5.0d);
		TransferenceDTO transferenceDTO2 = createTransferenceDTO(adrian, mark, 2.0d);
		accountDAO.transferMoney(transferenceDTO1);
		accountDAO.transferMoney(transferenceDTO2);
		Account updatedAccount = accountDAO.findAccount(mark.getAccountNumber());
    	assertTrue(updatedAccount.getCurrentValue().doubleValue() == 12.0d );
    }
	
	private AccountDTO createAccountDTO(String ownerName, String ownerDocument, double initialValue) {
		AccountDTO account = new AccountDTO();
		account.setAccountOwner(ownerName);
		account.setAccountOwnerId(ownerDocument);
		account.setCurrentValue(initialValue);
		return account;
	}
	
	private TransferenceDTO createTransferenceDTO(AccountDTO sender, AccountDTO receiver, double value) {
		TransferenceDTO transference = new TransferenceDTO();
		transference.setReceiverAccount(receiver.getAccountNumber());
		transference.setSenderAccount(sender.getAccountNumber());
		transference.setValue(value);
		
		return transference;
	}
}
